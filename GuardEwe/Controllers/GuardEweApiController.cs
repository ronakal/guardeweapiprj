﻿using Amazon.S3;
using Amazon.S3.Transfer;
using GuardEwe.Models;
using System.IO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;


namespace GuardEwe.Controllers
{
    public class GuardEweApiController : ApiController
    {
        public GuardEweApiController()
        {

        }

        [Route("api/GuardEweApi/UploadToS3")]
        [HttpGet]
        public List<Status> UploadToS3()
        {

            string[] files =
            Directory.GetFiles(@"C:\GuardEweLocationFiles", "*.txt", SearchOption.AllDirectories);
            List<Status> s = new List<Status>();
            foreach (var item in files)
            {
                var fileName=Path.GetFileName(item);

                var test2 = Path.GetDirectoryName(item);
                s.Add(sendMyFileToS3(item, "guardewe","", "GuardEwe"+fileName));
            }

            return s;
        }
        public Status sendMyFileToS3(string localFilePath, string bucketName, string subDirectoryInBucket, string fileNameInS3)
        {
            var s = new Status();
            // input explained :
            // localFilePath = the full local file path e.g. "c:\mydir\mysubdir\myfilename.zip"
            // bucketName : the name of the bucket in S3 ,the bucket should be alreadt created
            // subDirectoryInBucket : if this string is not empty the file will be uploaded to
            // a subdirectory with this name
            // fileNameInS3 = the file name in the S3
            // create a TransferUtility instance passing it the IAmazonS3 created 
            TransferUtility utility = new TransferUtility(new AmazonS3Client(Amazon.RegionEndpoint.USEast1));
            // making a TransferUtilityUploadRequest instance
            TransferUtilityUploadRequest request = new TransferUtilityUploadRequest();

            if (string.IsNullOrEmpty(subDirectoryInBucket) )
            {
                request.BucketName = bucketName; //no subdirectory just bucket name
            }
            else
            {   // subdirectory and bucket name
                request.BucketName = bucketName + @"/" + subDirectoryInBucket;
            }
            request.Key = fileNameInS3; //file name up in S3
            request.FilePath = localFilePath; //local file name
            try
            {
                utility.Upload(request); //commensing the transfer
                s.status = "OK";
                s.Message = "Successful";
                s.FileName = fileNameInS3;
            }
            catch (AmazonS3Exception amazonS3Exception)
            {
                if (amazonS3Exception.ErrorCode != null &&
                    (amazonS3Exception.ErrorCode.Equals("InvalidAccessKeyId")
                    ||
                    amazonS3Exception.ErrorCode.Equals("InvalidSecurity")))
                {
                    s.status = "Failed";
                    s.Message = "Check the provided AWS Credentials.";
                    s.FileName = fileNameInS3;
                }
                else
                {
                    s.status = "Failed";
                    s.Message = amazonS3Exception.Message;
                    s.FileName = fileNameInS3;
                }
            }


                return s; //indicate that the file was sent
        }

    }
}