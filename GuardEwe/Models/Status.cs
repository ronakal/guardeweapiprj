﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GuardEwe.Models
{
    public class Status
    {

        public string status { get; set; }
        public string Message { get; set; }
        public string FileName { get; set; }
    }
}